<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);
    register_nav_menus([
        'footer_navigation' => __('Footer Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));

    /**
     * ACF Options Page
     */
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page();
    }
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Custom AJAX to add product to cart
 */
add_action('wp_ajax_add_item_to_cart', function () {
    $product_id = $_POST['product_id'];
    $variation_id = $_POST['variation_id'] ?? '';
    $quantity = $_POST['quantity'];
    $promoType = $_POST['type'];  // 1 = First Step Promo (15%), 2 = Second Step Checkout (50%)

    $cart_item_data = [];
    if($promoType == 1) {
        $percentage = 0.15;
        $cart_item_data = ['custom_checkout_discounted_percentage' => $percentage, 'price_already_discounted' => false];
    }

    //TODO it doesn't add the right percentage
    if($promoType == 2) {
        $specialDiscountPercentage  = get_field('discount_step_2_checkout', 'option');
        if((float)WC()->cart->total < 50) {
            $specialDiscountPercentage = 30;
        } elseif((float)WC()->cart->total >= 50) {
            $specialDiscountPercentage = 50;
        }
        $percentage = $specialDiscountPercentage / 100;  //0.3, 0.5
        $cart_item_data = ['custom_checkout_before_submit_discounted_percentage' => $percentage, 'price_already_discounted' => false];
    }

    if ($variation_id) {
        WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, [], $cart_item_data );
        do_action( 'woocommerce_ajax_added_to_cart', $variation_id ); // the hook is must, because of pixel event AddToCart  -not working
    } else {
        WC()->cart->add_to_cart( $product_id, $quantity, 0, [], $cart_item_data);
        do_action( 'woocommerce_ajax_added_to_cart', $product_id); // the hook is must, because of pixel event AddToCart
    }

    return true;
});

add_action('wp_ajax_nopriv_add_item_to_cart', function () {
    $product_id = $_POST['product_id'];
    $variation_id = $_POST['variation_id'] ?? '';
    $quantity = $_POST['quantity'];
    $promoType = $_POST['type'];  // 1 = First Step Promo (15%), 2 = Second Step Checkout (50%)

    $cart_item_data = [];
    if($promoType == 1) {
        $percentage = 0.15;
        $cart_item_data = ['custom_checkout_discounted_percentage' => $percentage, 'price_already_discounted' => false];
    }

    //TODO it doesn't add the right percentage
    if($promoType == 2) {
        $specialDiscountPercentage  = get_field('discount_step_2_checkout', 'option');
        if((float)WC()->cart->total < 50) {
            $specialDiscountPercentage = 30;
        } elseif((float)WC()->cart->total >= 50) {
            $specialDiscountPercentage = 50;
        }
        $percentage = $specialDiscountPercentage / 100;  //0.3, 0.5
        $cart_item_data = ['custom_checkout_before_submit_discounted_percentage' => $percentage, 'price_already_discounted' => false];
    }

    if ($variation_id) {
        WC()->cart->add_to_cart( $product_id, $quantity, $variation_id );
        do_action( 'woocommerce_ajax_added_to_cart', $variation_id ); // the hook is must, because of pixel event AddToCart
    } else {
        WC()->cart->add_to_cart( $product_id, $quantity, 0, [], $cart_item_data);
        do_action( 'woocommerce_ajax_added_to_cart', $product_id); // the hook is must, because of pixel event AddToCart
    }

    return true;
});
