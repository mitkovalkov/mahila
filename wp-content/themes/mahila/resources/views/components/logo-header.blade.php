@php
    $logo = get_field('logo_header', 'option');
@endphp
<a class="brand-logo" href="{{ home_url('/') }}"><img src="{{ $logo }}" alt="{{ get_bloginfo('name', 'display') }}"></a>
