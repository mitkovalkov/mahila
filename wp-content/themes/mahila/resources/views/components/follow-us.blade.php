@php
    $follow_us = get_field('follow_us', 'option');
@endphp
@if($follow_us)
<div class="follow-us">
    <ul>
    @foreach($follow_us as $item)
        <li><a href="{{ $item['link']['url'] }}" target="{{ $item['link']['target'] }}"><img src="{{ $item['icon'] }}" alt="{{ $item['link']['title'] }}"></a></li>
    @endforeach
    </ul>
</div>
@endif
