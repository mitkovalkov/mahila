@extends('layouts.app')

@section('content')
@include('partials.page-header')
<div class="page-content">
    <div class="{{ is_cart() ? 'container-fluid' : 'container' }}">
        @while(have_posts()) @php the_post() @endphp
            @include('partials.content-page')
        @endwhile
    </div>
</div>
@endsection
