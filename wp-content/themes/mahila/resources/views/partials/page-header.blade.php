@php
    $data['id'] = get_the_ID();
    $page_title = get_field('page_title', $data['id']);
    $page_description = get_field('page_description', $data['id']);
    $page_bg_image = get_field('page_image', $data['id']);
    $page_bg_image_mobile = get_field('page_image_mobile', $data['id']);
@endphp

@if($page_title || $page_description || $page_bg_image)
<div class="page-header section" @if($page_bg_image) style="background-image: url('{{ $page_bg_image }}');" @endif>
    @if($page_bg_image_mobile)<div class="img-box"><img src="{{ $page_bg_image_mobile }}" alt="{{ $page_title }}"></div>@endif
    <div class="container-fluid">
        @if($page_title)<h1 class="page-header-title animated-item">{!! $page_title !!}</h1>@endif
        @if($page_description)
            <div class="text animated-item">{!! $page_description !!}</div>
        @endif
    </div>
</div>
@endif
