@php
    $all_rights_reserved = '©'.date('Y').' '.get_field('all_rights_reserved', 'option');
    $address_information = get_field('address_information', 'option');
    $footer_text = get_field('footer_text', 'option');
    $footer_image = get_field('footer_image', 'option');
    $instagram_feed = get_field('instagram_feed', 'option');
@endphp

@if($instagram_feed && !is_checkout() && !is_cart())
<div class="section section-instagram">
    <div class="simple-title animated-item-bottom">{{ __('Последвайте ни в инстаграм', 'mahila') }}</div>
    <div class="instagram-container">
        {!! do_shortcode($instagram_feed) !!}
    </div>
</div>
@endif

@if($footer_image)
<div class="img-box footer-image">
    <img src="{{ $footer_image }}" alt="Mahila">
</div>
@endif
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                @include('components.logo-footer')
            </div>
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col">
                    <div class="text">{!! $footer_text !!}</div>
                </div>
                <div class="col">
                    @if($address_information)
                    <div class="text">{!! $address_information !!}</div>
                    @endif
                    <div class="follow-us-content">
                        <div class="d-flex align-items-center">
                            <span class="mr-2 text-uppercase">{{ __('social media', 'mahila') }}:</span>
                            @include('components.follow-us')
                        </div>
                    </div>
                    @if($all_rights_reserved)
                    <div class="text">
                        <p>{!! $all_rights_reserved !!}</p>
                    </div>
                    @endif
                </div>
                <div class="col">
                    @if(has_nav_menu('footer_navigation'))
                        {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']) !!}
                    @endif
                    <div class="text"><p>Website created by <a href="https://cehub.bg/" target="_blank"><i>CEHUB</i></a></p></div>
                </div>
            </div>
        </div>
    </div>
</footer>
