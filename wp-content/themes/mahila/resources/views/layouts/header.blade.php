@php
    $cart_count = WC()->cart->get_cart_contents_count();
@endphp
<div class="header-wrapper">
    <header class="header">
        <div class="container-fluid d-flex flex-wrap align-items-center justify-content-between justify-content-md-between">
            @include('components.logo-header')
            <nav class="nav-primary js-main-navigation ml-auto">
                @if(has_nav_menu('primary_navigation'))
                    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
                    <div class="cart-link">
                        <div class="cart-title"><a href="{{ home_url('/cart') }}">{{ __('Количка', 'mahila') }}</a></div> 
                        @if($cart_count > 0)<div class="cart-count">{{ $cart_count }}</div> @endif
                    </div>
                @endif
            </nav>
            @include('components.follow-us')
            <div class="toggle-main-navigation js-toggle-main-navigation">
                <div class="toggle-menu-inner"></div>
            </div>
        </div>
    </header>
</div>
