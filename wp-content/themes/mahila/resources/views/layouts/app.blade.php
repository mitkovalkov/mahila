<!doctype html>
<html {!! get_language_attributes() !!}>
    @include('layouts.head')
    <body @php body_class() @endphp>
        @php do_action('get_header') @endphp

        @include('layouts.header')

        <main class="main">
            @yield('content')
        </main>

        @php do_action('get_footer') @endphp

        @include('layouts.footer')

        @php wp_footer() @endphp
    </body>
</html>
