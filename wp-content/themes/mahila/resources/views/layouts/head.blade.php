<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @php wp_head() @endphp

    @php
        $js_code = get_field('js_code_inside_head_tag', 'option'); 
    @endphp
    @if($js_code)
        {!! $js_code !!}
    @endif
</head>
