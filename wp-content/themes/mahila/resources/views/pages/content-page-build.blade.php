@php
    $site_background = get_field('site_background', 'option');
@endphp
@if($data['page_build'])
    @foreach($data['page_build'] as $row)
        @if($row['acf_fc_layout'] == 'block_main_banner')
            @include('blocks.block-main-banner')
        @endif
    @endforeach
    
    <div class="section-products-information" style="background-image: url({{ $site_background }});">
        @foreach($data['page_build'] as $key => $row)

            @if($row['acf_fc_layout'] == 'products_listing')
                @include('blocks.block-products-listing')
            @endif

            @if($row['acf_fc_layout'] == 'block_tabs')
                @include('blocks.block-tabs')
            @endif

            @if($row['acf_fc_layout'] == 'block_text_and_image')
                @include('blocks.block-text-and-image')
            @endif

            @if($row['acf_fc_layout'] == 'block_info')
                @include('blocks.block-info')
            @endif

            @if($row['acf_fc_layout'] == 'block_simple_text')
                @include('blocks.block-simple-text')
            @endif

        @endforeach
    </div>

    @foreach($data['page_build'] as $row)
        @if($row['acf_fc_layout'] == 'block_about')
            @include('blocks.block-about')
        @endif

        @if($row['acf_fc_layout'] == 'block_images')
            @include('blocks.block-images')
        @endif
    @endforeach
@endif
