{{--
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */
--}}

@php
	defined( 'ABSPATH' ) || exit;

	global $product;

	// Ensure visibility.
	if ( empty( $product ) || ! $product->is_visible() ) {
		return;
	}

	$product_label = get_field('product_label', $product->get_id());
    $link = get_permalink($product->get_id());
    $product->image = get_the_post_thumbnail_url($product->get_id());

	@endphp

<li @php wc_product_class('', $product); @endphp>
	<div class="product-item">
		<div class="product-item-image">
			<div class="img-box">
				<a href="{{ $link }}"><img src="{{ $product->image }}" alt="{{ $product->get_title() }}"></a>
			</div>
		</div>
		<div class="product-info">
			<div class="product-item-title text-center">
				<div class="product-item-label"><a href="{{ $link }}">Mahila {{ $product_label }}</a></div>
				<h2><a href="{{ $link }}">{!! $product->get_title() !!}</a></h2>
				@php
					/**
					* Hook: woocommerce_after_shop_loop_item_title.
					*
					* @hooked woocommerce_template_loop_rating - 5
					* @hooked woocommerce_template_loop_price - 10
					*/
					// do_action( 'woocommerce_after_shop_loop_item_title' );
				@endphp
			</div>
			<div class="product-price">
				{!! $product->get_price_html() !!}
			</div>
			<div class="product-item-buttons">
				@php
					/**
					* Hook: woocommerce_after_shop_loop_item.
					*
					* @hooked woocommerce_template_loop_product_link_close - 5
					* @hooked woocommerce_template_loop_add_to_cart - 10
					*/
					do_action( 'woocommerce_after_shop_loop_item' );
				@endphp
			</div>
		</div>
	</div>
</li>
