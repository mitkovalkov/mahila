@php
/**
* The template for displaying product content in the single-product.php template
*
* This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see     https://docs.woocommerce.com/document/template-structure/
* @package WooCommerce\Templates
* @version 3.6.0
*/

	defined( 'ABSPATH' ) || exit;

	global $product;

	/**
	* Hook: woocommerce_before_single_product.
	*
	* @hooked woocommerce_output_all_notices - 10
	*/
	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
		echo get_the_password_form(); // WPCS: XSS ok.
		return;
	}

    $page_bg_image = get_field('product_image', $product->get_id());
    $page_bg_image_mobile = get_field('product_image_mobile', $product->get_id());
    $product_bg_image = get_field('product_image_background', $product->get_id());
    $product_icons = get_field('product_icons', $product->get_id());
    $product_images = get_field('product_images', $product->get_id());
	$product->image = get_the_post_thumbnail_url($product->get_id());
	$product_label = get_field('product_label', $product->get_id());
	$product_info = get_field('product_info_block', $product->get_id());
@endphp
@if($page_bg_image)
<div class="page-header" style="background-image: url('{{ $page_bg_image }}');">
	@if($page_bg_image_mobile)<div class="img-box"><img src="{{ $page_bg_image_mobile }}" alt="Mahila"></div>@endif
</div>
@endif
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'product-detail-content', $product ); ?> @if($product_bg_image) style="background-image: url('{{ $product_bg_image }}');" @endif>
	<div class="product-detail-content-left">
		<h3 class="product-label">Mahila {{ $product_label }}</h3>
		@php
			wc_get_template( 'single-product/title.php' );
		@endphp
		<div class="text mt-3 mb-5">
			{!! the_content() !!}
		</div>
	</div>
	<div class="product-detail-content-center">
		<div class="img-box">
			<img src="{{ $product->image }}" alt="Mahila">
		</div>
	</div>
	<div class="product-detail-content-right">
		@php
			wc_get_template( 'single-product/rating.php' );
			wc_get_template( 'single-product/price.php' );
		@endphp

		@php
			do_action('woocommerce_' . $product->get_type() . '_add_to_cart');
		@endphp

		@if($product_info['text_right'])
		<div class="text">{!! $product_info['text_right'] !!}</div>
		@endif

		@if($product_icons)
			<div class="product-detail-content-icons">
			@foreach($product_icons as $item)
				<div class="icon-box">
					<div class="icon"><img src="{{ $item['icon'] }}" alt="{{ $item['text'] }}"></div>
					<div class="text"><p>{{ $item['text'] }}</p></div>
				</div>
			@endforeach
			</div>
		@endif
	</div>
	<div class="product-detail-content-bottom">
	@if($product_info['text'])
		<div class="section section-product-info" @if($product_info['image']) style="background-image: url('{{ $product_info['image'] }}');" @endif>
			@if($product_info['image_mobile'])<div class="img-box"><img src="{{ $product_info['image_mobile'] }}" alt="Mahila"></div>@endif
			<div class="container-fluid">
				<div class="text">
					{!! $product_info['text'] !!}
				</div>
			</div>
		</div>
	@endif
		@php
			/**
			* Hook: woocommerce_after_single_product_summary.
			*
			* @hooked woocommerce_output_product_data_tabs - 10
			* @hooked woocommerce_upsell_display - 15
			* @hooked woocommerce_output_related_products - 20
			*/
			do_action( 'woocommerce_after_single_product_summary' );
		@endphp
	</div>
</div>
@if($product_images)
<div class="section section-product-images">
	<div class="container-fluid">
        <div class="swiper-images swiper-container">
            <div class="swiper-wrapper">
			@foreach($product_images as $item)
				<div class="swiper-slide">
					<div class="img-box">
						<div class="circle-img-box"><img src="@asset('images/circle.png')" alt="Mahila" class="cirle-img"></div>
						<img src="{{ $item['image'] }}" alt="Mahila">
					</div>
				</div>
			@endforeach
			</div>
		</div>
	</div>
</div>
@endif

@php
	do_action( 'woocommerce_after_single_product' );
@endphp
