<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/*
 * @hooked wc_empty_cart_message - 10
 */
do_action( 'woocommerce_cart_is_empty' );

if ( wc_get_page_id( 'shop' ) > 0 ) : ?>
<div class="section section-cart">
	<div class="row">
		<div class="col pt-5 pb-5">
			<p class="return-to-shop text-center">
				<a class="button wc-backward btn btn-primary" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
					<?php
						/**
						 * Filter "Return To Shop" text.
						 *
						 * @since 4.6.0
						 * @param string $default_text Default text.
						 */
						echo esc_html( apply_filters( 'woocommerce_return_to_shop_text', __( 'Към магазина', 'mahila' ) ) );
					?>
				</a>
			</p>
		</div>
	</div>
</div>
<div class="section section-product-related">
	<div class="container">
		<div class="simple-title text-center animated-item-bottom">{!! __('Може също да харасете', 'mahila') !!}</div>
		<div class="swiper-product-related swiper-container">
			<div class="swiper-wrapper">
				@php
					$args = array(
						'post_type'      => 'product',
						'posts_per_page' => 5,
						'product_cat'    => 'uncategorized',
						'order'          => 'DESC',
						'orderby'        => 'menu_order',
					);

					$loop = new WP_Query( $args );
				@endphp

				@foreach($loop->posts as $product)
                    @php
                        setup_postdata($product);
                    @endphp
                    <div class="swiper-slide">
                        @include('woocommerce.content-product', ['product' => $product->ID])
                    </div>
                @endforeach
			</div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
		</div>
	</div>
</div>
<?php endif; ?>
