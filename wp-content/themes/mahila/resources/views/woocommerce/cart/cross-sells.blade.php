@php
	/**
	* Cross-sells
	*
	* This template can be overridden by copying it to yourtheme/woocommerce/cart/cross-sells.php.
	*
	* HOWEVER, on occasion WooCommerce will need to update template files and you
	* (the theme developer) will need to copy the new files to your theme to
	* maintain compatibility. We try to do this as little as possible, but it does
	* happen. When this occurs the version of the template file will be bumped and
	* the readme will list any important changes.
	*
	* @see https://docs.woocommerce.com/document/template-structure/
	* @package WooCommerce\Templates
	* @version 4.4.0
	*/

	defined( 'ABSPATH' ) || exit;
@endphp

@if($cross_sells)
<div class="section section-product-related">
	<div class="container">
		<div class="cross-sells">
			@php
				$heading = apply_filters( 'woocommerce_product_cross_sells_products_heading', __( 'Може също да харасете', 'mahila' ) );
			@endphp

			@if($heading)
				<div class="simple-title text-center animated-item-bottom">{!! $heading !!}</div>
			@endif

			<div class="swiper swiper-product-related swiper-container">
				<div class="swiper-wrapper">
				@foreach($cross_sells as $cross_sell)
					@php
						$post_object = get_post( $cross_sell->get_id() );
						setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

						echo '<div class="swiper-slide">';
						wc_get_template_part( 'content', 'product' );
						echo '</div>';
					@endphp
				@endforeach
				</div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
			</div>
		</div>
	</div>
</div>
@endif

@php
	wp_reset_postdata();
@endphp
