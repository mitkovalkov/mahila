<div class="section section-about {{ $row['background_color'] ? 'has-baground-color' : '' }}"
    @if($row['section_id']) id="{{ $row['section_id'] }}" @endif
    @if($row['background_color']) style="background-color: {{ $row['background_color'] }}" @endif
>
    <div class="container-fluid">
        <div class="row align-items-center">
            @if($row['image'])
            <div class="col-12 col-md-7">
                <div class="img-box animated-item">
                    @if($row['button'])
                        <a href="{{ $row['button']['url'] }}"><img src="{{ $row['image'] }}" alt="{{ $row['title'] }}"></a>
                    @else
                        <img src="{{ $row['image'] }}" alt="{{ $row['title'] }}">
                    @endif
                </div>
            </div>
            @endif
            <div class="col-12 col-md-5">
                <div class="text-content text-center">
                    @if($row['title'])
                    <div class="main-title-container">
                        <h3 class="main-title animated-item-bottom">{!! $row['title'] !!}</h3>
                    </div>
                    @endif
                    @if($row['subtitle'])
                        <h4 class="section-subtitle animated-item-bottom">{!! $row['subtitle'] !!}</h4>
                    @endif
                    @if($row['text'])
                    <div class="text text-center animated-item-bottom">{!! $row['text'] !!}</div>
                    @endif
                    @if($row['button'])
                        <div class="button-container animated-item-bottom">
                            <a href="{{ $row['button']['url'] }}" class="btn btn-primary btn-sm" target="{{ $row['button']['target'] }}">{{ $row['button']['title'] }}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>