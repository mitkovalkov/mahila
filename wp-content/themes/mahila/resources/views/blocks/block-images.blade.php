<div class="section section-images" @if($row['section_id']) id="{{ $row['section_id'] }}" @endif)>
    <div class="container-fluid">
        <div class="swiper-images swiper-container">
            <div class="swiper-wrapper">
            @if($row['images'])
                @foreach($row['images'] as $item)
                <div class="swiper-slide">
                    <div class="img-box">
                        @if($item['image'])<img src="{{ $item['image'] }}" alt="Mahila" loading="lazy">@endif
                        @if($item['video'])<video src="{{ $item['video'] }}" muted playsinline loop autoplay></video>@endif
                    </div>
                </div>
                @endforeach
            @endif
            </div>
        </div>
    </div>
</div>
