<div class="section section-info">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-5">
                @if($row['image'])<div class="img-box"><img src="{{ $row['image'] }}" alt="{{ $row['title'] }}"></div>@endif
            </div>
            <div class="col-md-7">
                <div class="text">
                    @if($row['title'])
                    <div class="title animated-item-bottom">{{ $row['title'] }}</div>
                    @endif
                    @if($row['text'])
                    <div class="subtitle animated-item-bottom">{!! $row['text'] !!}</div>
                    @endif
                    <div class="img-cream animated-item"><img src="@asset('images/cream.png')" alt="Mahila"></div>
                </div>
            </div>
        </div>
    </div>
</div>