<div class="section section-product-related" @if($row['section_id']) id="{{ $row['section_id'] }}" @endif)>
    <div class="container">
        @if($row['title'])<div class="simple-title text-center animated-item-bottom">{!! $row['title'] !!}</div>@endif
        @if($row['text'])<div class="text text-center animated-item-bottom">{!! $row['text'] !!}</div>@endif
        <div class="swiper-product-related swiper-container">
            <div class="swiper-wrapper">
                @foreach($row['products'] as $product)
                    @php
                        setup_postdata($product);
                    @endphp
                    <div class="swiper-slide">
                        @include('woocommerce.content-product', ['product' => $product->ID])
                    </div>
                @endforeach
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</div>
