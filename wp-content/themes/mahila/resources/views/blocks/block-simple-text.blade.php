@php 
    $has_background_image = $row['background_image'];
@endphp
<div class="section section-simple-text {{ $has_background_image ? 'has-bg-image' : '' }}" 
    @if($has_background_image) style="background-image: url({{ $has_background_image }});" @endif
    @if($row['section_id']) id="{{ $row['section_id'] }}" @endif)>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="text-content text-@if($row['content_position']){{ $row['content_position'] }}@endif">
                    @if($row['title'])
                    <div class="main-title-container animated-item-bottom">
                        <h2 class="main-title">{!! $row['title'] !!}</h2>
                    </div>
                    @endif
                    @if($row['subtitle'])<h4 class="section-subtitle animated-item-bottom">{!! $row['subtitle'] !!}</h4>@endif
                    @if($row['text'])<div class="text animated-item-bottom">{!! $row['text'] !!}</div>@endif
                    @if($row['button'])
                        <div class="button-container animated-item-bottom">
                            <a href="{{ $row['button']['url'] }}" class="btn btn-primary btn-sm" target="{{ $row['button']['target'] }}">{{ $row['button']['title'] }}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>