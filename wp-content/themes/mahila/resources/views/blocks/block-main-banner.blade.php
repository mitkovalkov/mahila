<div class="section section-main-banner">
    @if($row['block_main_banner_video'])
        <video src="{{ $row['block_main_banner_video'] }}" muted playsinline loop autoplay></video>
    @endif
    <div class="container-fluid">
        @foreach($row['block_main_banner'] as $item)
            <div class="row">
                <div class="main-banner-item">
                    @if($item['image'] && empty($row['block_main_banner_video']))<div class="background-image" style="background-image: url({{ $item['image'] }});"></div>@endif
                    <div class="main-banner-container">
                        @if($item['title'])
                            <h3 class="main-banner-item-title animated-item-bottom"><img src="@asset('images/new-label.png')" alt="Mahila" data-ios="fade-in">{!! $item['title'] !!}</h3>
                        @endif
                        @if($item['text'])
                        <div class="text animated-item-bottom">
                            {!! $item['text'] !!}
                        </div>
                        @endif
                        @if($item['button'])
                            <div class="button-container animated-item-bottom">
                                <a href="{{ $item['button']['url'] }}" class="btn btn-primary" target="{{ $item['button']['target'] }}">{{ $item['button']['title'] }}</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="keep-scrolling animated-item-bottom"><span>keep scrolling</span></div>
</div>
