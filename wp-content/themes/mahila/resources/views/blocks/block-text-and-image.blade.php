<div class="section section-text-and-image {{ $row['background_color'] ? 'has-baground-color' : '' }}"
    @if($row['section_id']) id="{{ $row['section_id'] }}" @endif
    @if($row['background_color']) style="background-color: {{ $row['background_color'] }}; background-image: url({{ $row['image']['url'] }})" @endif
>
    <div class="container-fluid">
        <div class="row align-items-center">
            @if($row['image'])
            <div class="col-12 col-md-5">
                <div class="img-box">
                    @if($row['button'])
                        <a href="{{ $row['button']['url'] }}"><img src="{{ $row['image']['url'] }}" alt="{{ $row['image']['title'] }}"></a>
                    @else
                        <img src="{{ $row['image']['url'] }}" alt="{{ $row['image']['title'] }}">
                    @endif
                </div>
            </div>
            @endif
            <div class="col-12 col-md-7">
                <div class="text-content">
                    @if($row['title'])
                    <div class="main-title-container">
                        <h3 class="main-title animated-item-bottom">{!! $row['title'] !!}</h3>
                    </div>
                    @endif
                    @if($row['subtitle'])
                        <div class="counter" data-aos="fade-in" data-aos-delay="300">
                            <h4 class="section-subtitle">{!! $row['subtitle'] !!}</h4>
                        </div>
                    @endif
                    @if($row['text'])
                        <div class="text animated-item-info-bottom">{!! $row['text'] !!}</div>
                    @endif
                    @if($row['button'])
                        <div class="button-container animated-item-bottom">
                            <a href="{{ $row['button']['url'] }}" class="btn btn-primary btn-sm" target="{{ $row['button']['target'] }}">{{ $row['button']['title'] }}</a>
                        </div>
                    @endif
                </div>
                @if($row['item_repeater'])
                    <div class="item-repeater">
                        @foreach($row['item_repeater'] as $item)
                            <div class="item animated-item-bottom">
                                <div class="icon"><img src="{{ $item['icon'] }}" alt=""></div>
                                <div class="item-content">
                                    <div class="title">{{ $item['title'] }}</div>
                                    <div class="text">{!! $item['text'] !!}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
                @if($row['text_bottom'])
                    <div class="text text-bottom animated-item-bottom">{!! $row['text_bottom'] !!}</div>
                @endif
            </div>
        </div>
    </div>
</div>
