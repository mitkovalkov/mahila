<div class="section section-tabs" id="section-services">
    <div class="container-fluid">
        <div class="row">
            @if($row['title'])
            <div class="col-12">
                <div class="main-title-container">
                    <h3 class="main-title animated-item-bottom animated-item-bottom">{!! $row['title'] !!}</h3>
                </div>
            </div>
            @endif
            @if($row['tabs'])
            <div class="col-12 {{ $row['image'] ? 'col-md-7' : 'col-md-12' }}">
                <div class="tabs-container animated-item">
                    <div class="tabs-nav">
                        @foreach($row['tabs'] as $key => $tab)
                            <div class="tab js-tab animated-item-bottom {{ $key === 0 ? 'is-active' : '' }}" data-id="tab-{{ $key }}" @if($tab['image']) data-image="{{ $tab['image'] }}" @endif>
                                {!! $tab['title'] !!}
                            </div>
                        @endforeach
                    </div>
                    <div class="tabs-content">
                        @foreach($row['tabs'] as $key => $tab)
                            <div class="tab-container js-tab-container" id="tab-{{ $key }}" @if($key === 0) style="display: block;" @endif>
                                <div class="text text-justify animated-item-info-bottom">{!! $tab['text'] !!}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @if($row['image'])
                <div class="col-12 col-md-5">
                    <div class="img-box"><img src="{{ $row['image'] }}" alt="{{ $row['title'] }}" class="js-image-swap"></div>
                </div>
            @endif
            @endif
        </div>
    </div>
</div>
