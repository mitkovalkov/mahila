export const SWIPER_OPTIONS_RELATED_PRODUCTS = {
    slidesPerView: 3,
    spaceBetween: 80,
    observer: true,
    observeParents: true,
    loop: true,
    centeredSlides: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 1,
            spaceBetween: 40,
        },
        768: {
            slidesPerView: 2,
        },
        1280: {
            slidesPerView: 3,
        },
    },
};

export const SWIPER_OPTIONS_IMAGES = {
    slidesPerView: 3,
    spaceBetween: 40,
    observer: true,
    observeParents: true,
    loop: true,
    centeredSlides: true,
    breakpoints: {
        0: {
            autoplay: {
                delay: 5000,
            },
            slidesPerView: 1,
            spaceBetween: 40,
        },
        768: {
            autoplay: {
                delay: 5000,
            },
            slidesPerView: 2,
        },
        1280: {
            slidesPerView: 3,
        },
    },
}
