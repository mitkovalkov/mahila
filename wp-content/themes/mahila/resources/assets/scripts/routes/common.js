import SwiperCarousel from '../modules/SwiperCarousel';
import SwiperImages from '../modules/SwiperImages';
import Tabs from '../modules/Tabs';
import Animations from '../modules/Animations';
import ToggleMainNavigation from '../modules/ToggleMainNavigation';
import ImageSwap from '../modules/ImageSwap';
import ScrollToSection from '../modules/ScrollToSection';
import QuantityInput from '../modules/QuantityInput';
import AddToCart from '../modules/AddToCart';

export default {
    init() {
        // JavaScript to be fired on all pages
        new Animations;
        new Tabs;
        new ToggleMainNavigation;
        new ImageSwap;
        new ScrollToSection;
        new QuantityInput;
        new AddToCart;

        $(window).on('load', function() {
            new SwiperCarousel;
            new SwiperImages;
        });

        $( 'a[href*="#"]:not([href="#"])' ).on('click', function(e) {
            const target = $($(this).attr('href'));
            const headerHeight = $('.header').height();

            if (target.length) {
                e.preventDefault();

                $('body, html').animate({
                    scrollTop: target.offset().top - (headerHeight + 20),
                }, 500);
            }
        });
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
