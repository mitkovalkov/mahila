import SwiperCarousel from '../modules/SwiperCarousel';

export default {
    init() {
        $(document).on('change keyup focusout', '.qty', function () {
            const udpateCartButton = $('[name=update_cart]');
            udpateCartButton.attr('disabled', false);
            udpateCartButton.trigger('click');
        });

        $(document.body).on('wc_cart_emptied', function(event) {
            if (event) {
                new SwiperCarousel;
                $('.cart-count').remove();
            }
        }).on('updated_cart_totals', function() {
            const cartCount = $('.cart-count');
            let count = 0;

            $('.qty').each(function() {
                const $this = $(this);
                const qtyValue = Number($this.val());

                count += qtyValue;
            });

            cartCount.text(count);
        });
    },
};
  