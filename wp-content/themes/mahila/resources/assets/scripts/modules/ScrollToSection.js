export default class ScrollToSection {
    constructor () {
        this.$href = $('a[title]');
        this.scrollOffset = 150;
        this._initScroll();

        if ($(window).width() < 685) {
            this.scrollOffset = -330;
        }
    }

    _initScroll () {
        this.$href.on('click', this.handleHrefClick.bind(this))
        this.scrollToSection()
    }

    handleHrefClick (e) {
        const $this = $(e.currentTarget);
        const sectionLocation = $this.attr('title');
        const url = $this.attr('href');

        if ($('body').hasClass('home') && sectionLocation) {
            $('html, body').animate({ scrollTop: $(`${sectionLocation}`).offset().top - this.scrollOffset }, 1000);
            return false;
        } else {
            localStorage.setItem('section-location', sectionLocation);
        }

        window.location = url;
    }

    scrollToSection () {
        const activeSection = localStorage.getItem('section-location');

        if(activeSection) {
            setTimeout(() => {
                $('html, body').animate({ scrollTop: $(`${activeSection}`).offset().top - this.scrollOffset }, 1000);
                localStorage.removeItem('section-location');
            }, 1000);
        }
    }
}