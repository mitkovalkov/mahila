export default class ImageSwap {
    constructor () {
        this.tab = $('.js-tab');
        this.init();
    }

    init () {
        this.tab.on('click', this.imageSwap.bind(this));
    }

    imageSwap(e) {
        const $this = $(e.currentTarget);
        const imageUrl = $this.data('image');
        const imageSwap = $('.js-image-swap');

        imageSwap.fadeTo(500, 0.20, function() {
            imageSwap.attr('src', imageUrl);
        }).fadeTo(500, 1);
        return false;
    }
}
