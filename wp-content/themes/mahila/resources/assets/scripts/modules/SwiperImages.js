import Swiper from 'swiper';
import { SWIPER_OPTIONS_IMAGES } from '../configs/swiper.config';

export default class SwiperCarousel {
    constructor () {
        this.swiperContainerImages = '.swiper-images';

        this._initSwipers();
    }

    _initSwipers () {
        new Swiper(this.swiperContainerImages, SWIPER_OPTIONS_IMAGES)
    }
}
