export default class QuantityInput {
    constructor () {
        this.quantityBtn = '.quantity-btn';
        this.init();
    }

    init () {
        $(document).on('click', this.quantityBtn, this.handleQuantityUpdate.bind(this));
    }

    handleQuantityUpdate(e) {
        const $this = $(e.currentTarget);
        const parent = $this.parents('.quantity');
        const quantityInput = parent.find('input.qty');
        let count = Number(quantityInput.val());

        if ($this.is('.btn-increment')) {
            quantityInput.val(count += 1);
            quantityInput.trigger('change');
            $('[name=update_cart]').attr('disabled', false).trigger('click');
        } else {
            if (quantityInput.val() > 1) {
                quantityInput.val(count -= 1)
                quantityInput.trigger('change');
            }

            $('[name=update_cart]').attr('disabled', false).trigger('click')
        }
    }
}
