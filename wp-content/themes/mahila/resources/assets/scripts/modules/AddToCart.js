/* eslint-disable */
export default class AddToCart {
    constructor () {
        this._init();
    }

    _init () {
        $(document.body).on('added_to_cart', function(event, fragments, cart_hash, $button) {
            const cartCountContainer = $('.cart-link');
            const cartCount = cartCountContainer.find('.cart-count');
            const button = $button[0];
            const buttonQty = $(button).data('quantity');
            let count = Number(buttonQty);

            if (cartCount.length) {
                count += Number(cartCount.text());
                cartCount.text(count);
            } else {
                cartCountContainer.append(`<div class="cart-count">${buttonQty}</div>`);
            }
        });
    }
}
