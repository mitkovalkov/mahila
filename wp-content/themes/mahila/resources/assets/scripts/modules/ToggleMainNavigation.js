export default class ToggleMainNavigation {
    constructor () {
        this.toggleButton = $('.js-toggle-main-navigation');
        this.init();
    }

    init () {
        this.toggleButton.on('click', this.toggleMainNavigation);
        $('.js-main-navigation a').on('click', this.navigationClick);
    }

    toggleMainNavigation () {
        const button = $(this);

        button.toggleClass('is-active');
        $('.js-main-navigation').toggleClass('is-active');
    }

    navigationClick () {
        setTimeout(() => {
            $('.js-main-navigation, .js-toggle-main-navigation').removeClass('is-active');
        }, 200);
    }
}
