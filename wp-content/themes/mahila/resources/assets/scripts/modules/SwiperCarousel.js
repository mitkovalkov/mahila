import Swiper from 'swiper';
import { SWIPER_OPTIONS_RELATED_PRODUCTS } from '../configs/swiper.config';

export default class SwiperCarousel {
    constructor () {
        this.swiperContainerRelated = '.swiper-product-related';

        this._initSwipers();
    }

    _initSwipers () {
        new Swiper(this.swiperContainerRelated, SWIPER_OPTIONS_RELATED_PRODUCTS)
    }
}