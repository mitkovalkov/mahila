import AOS from 'aos';
import { TweenMax, TimelineMax, Power2 } from 'gsap'
import ScrollMagic from 'scrollmagic';
import { ScrollMagicPluginGsap } from 'scrollmagic-plugin-gsap'
ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax)

export default class Animations {
    
    constructor () {
        this._section = '.section';
        this._initAnimation();
    }

    _initAnimation () {
        $(window).on('load', function() {
            setTimeout(() => {
                AOS.init({
                    mirror: false,
                    once: true,
                });
            }, 1);
        });

        let ctrl = new ScrollMagic.Controller();
        $(this._section).each(function() {
            let animatedItem = $(this).find('.animated-item')
            let animatedItemBottom = $(this).find('.animated-item-bottom')
            let animatedItemInfoBottom = $(this).find('.animated-item-info-bottom *')
            let timeLine = new TimelineMax();

            timeLine.staggerFromTo(animatedItem, 1, { opacity: 0 }, {opacity: 1, ease: Power2.easeInOut}, .3, .35)
            timeLine.staggerFromTo(animatedItemBottom, 1, { opacity: 0, y: '80px' }, {opacity: 1, y: '0', ease: Power2.easeInOut}, .3, .35)
            timeLine.staggerFromTo(animatedItemInfoBottom, 1, { opacity: 0, y: '80px' }, {opacity: 1, y: '0', ease: Power2.easeInOut}, .3, .35)
            
            new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 0.94,
                reverse: !1,
            }).setTween(timeLine).addTo(ctrl);
        });
    }
}
