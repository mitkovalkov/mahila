<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mahila' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'zyjBmuSofsFHOLqyH1cIIlawRlZM7cOu00ZMuIfid9QTPEBgE4u5EqEFVP9e5IVd' );
define( 'SECURE_AUTH_KEY',  'Q3yVRBtdEJNVwxCjenyuI5J8z278heEIXLfvdE3jM3Gqv7S5b8WhbxpSR9RykrzY' );
define( 'LOGGED_IN_KEY',    'F5Fbn2rSe03cG3iJ2DCJvGtRMEvLFet6z8ru2uVwjzIoW525ErGn3ePL7Xi2hkqj' );
define( 'NONCE_KEY',        'GVaWHXi2da2Ed5CvXKyyjplB0emsdDYw5R56AT0zRBolhohUFHWMgs686O533hZ3' );
define( 'AUTH_SALT',        'hHOXcxkW915PD30v2br3LSNKxghAVHu11lnYhdRNs5rA8tjUzgTKUcIRts1g0eu6' );
define( 'SECURE_AUTH_SALT', 'IkLDHS9ONy03bUvoOAyFuYjC6ChOmKFRSoEhZQCp3KSYTJYjNzAk19VQDVPxQz5Z' );
define( 'LOGGED_IN_SALT',   'hh5ANOkxGJOCPENLpCTMNyaUjwqmCCwCpoKEhe9wCRpEj59bKgSPJwSeM99YhNcp' );
define( 'NONCE_SALT',       '7S875VgWZaCXD0X0CUEQg9K4RwNxv6Ibegt0K8cAeP7T5vi2dLm8LOksSNoJcOpQ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
